package counter

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"test-task/internal/pkg/storage"
)

type server struct {
	httpSrv *http.Server
	log     *log.Logger

	interval time.Duration
	cache    CacheI
	storage  storage.StorageI
}

func (s *server) handleRequest(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, s.cache.Size())
}

func (s *server) counterMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		nowTime := time.Now().UnixNano()
		s.cache.Update()
		// Сlear the storage if irrelevant data is stored in it
		if s.cache.Size() == 0 {
			s.storage.Clear()
		}
		if err := s.storage.Insert(nowTime); err != nil {
			s.log.Printf("Can't insert data in storage %+v\n", err)
		}
		s.cache.Add(nowTime)
		next.ServeHTTP(w, r)
	})
}

// NewServer returns server
func NewServer(port string, readTimeout time.Duration, writeTimeout time.Duration, logger *log.Logger, interval time.Duration, storage storage.StorageI) *server {
	// Init server
	srv := &server{
		httpSrv: &http.Server{
			Addr:         port,
			ReadTimeout:  readTimeout,
			WriteTimeout: writeTimeout,
		},
		log:      logger,
		interval: interval,
		cache:    NewCache(interval),
		storage:  storage,
	}
	// Init handler and middleware
	mux := http.NewServeMux()
	mux.HandleFunc("/", srv.handleRequest)
	srv.httpSrv.Handler = srv.counterMiddleware(mux)
	return srv
}

// InitCache puts the actual data in the cache
func (s *server) InitCache() {
	s.log.Println("Initialize cache")
	data, err := s.storage.GetSince(time.Now().UnixNano() - s.interval.Nanoseconds())
	if err != nil {
		s.log.Printf("Can't get data from storage %+v\n", err)
		return
	}
	for i := range data {
		s.cache.Add(data[i])
	}
}

// Start starts server
func (s *server) Start() error {
	s.log.Println("Starting server")
	return s.httpSrv.ListenAndServe()
}

// Shutdown turns off the server
func (s *server) Shutdown(ctx context.Context) error {
	return s.httpSrv.Shutdown(ctx)
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "URL:", r.URL.String())
}
