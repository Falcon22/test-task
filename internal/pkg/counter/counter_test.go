package counter

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"
)

type StorageMock struct{}

func (*StorageMock) Insert(int64) error {
	return nil
}

func (*StorageMock) GetSince(int64) ([]int64, error) {
	return []int64{}, nil
}

func (*StorageMock) Clear() error {
	return nil
}

func CreateMockServer(interval time.Duration) *server {
	srv := &server{
		&http.Server{},
		log.New(ioutil.Discard, "", log.Lshortfile),
		interval,
		NewCache(interval),
		&StorageMock{},
	}

	// Init handler and middleware
	mux := http.NewServeMux()
	mux.HandleFunc("/", srv.handleRequest)
	srv.httpSrv.Handler = srv.counterMiddleware(mux)

	return srv
}

func TestRequest(t *testing.T) {
	srv := CreateMockServer(time.Hour)
	srv.InitCache()
	ts := httptest.NewServer(srv.httpSrv.Handler)
	req, err := http.NewRequest(http.MethodGet, ts.URL, nil)
	if err != nil {
		t.Errorf("unexpected error: %#v", err)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Errorf("unexpected error: %#v", err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Errorf("unexpected http status: %s", resp.Status)
	}
}

func TestCounter(t *testing.T) {
	ts := httptest.NewServer(CreateMockServer(time.Hour * 24).httpSrv.Handler)
	req, err := http.NewRequest(http.MethodGet, ts.URL, nil)
	if err != nil {
		t.Errorf("unexpected error: %#v", err)
	}

	client := &http.Client{}

	for i := 0; i < 1000; i++ {
		resp, err := client.Do(req)
		if err != nil {
			t.Errorf("unexpected error: %#v", err)
		}
		if resp.StatusCode != http.StatusOK {
			t.Errorf("unexpected http status: %s", resp.Status)
		}

		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Errorf("unexpected error: %#v", err)
		}
		str := string(body)
		str = strings.TrimSuffix(str, "\n")
		value, err := strconv.Atoi(string(str))
		if err != nil {
			t.Errorf("unexpected error: %#v", err)
		}
		if value != i+1 {
			t.Errorf("unexpected count: %d expected: %d", value, i)
		}
	}
}
