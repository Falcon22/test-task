# Test task for Kaspersky Lab
A server which, for each request, returns a counter value that counts the total number of requests processed by the server in the last 60 seconds. <br>
Data is stored on local storage and cached in memory.
## Usage
### Local run
`./scripts/run_local_counter.sh`
### Run tests
`./scripts/run_tests_counter.sh`
### Deploy
`./scripts/up_counter.sh`
### Start service
`./scripts/start_counter.sh`
### Stop service
`./scripts/stop_counter.sh`
### Restart service
`./scripts/restart_counter.sh`
