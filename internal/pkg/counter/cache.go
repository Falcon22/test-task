package counter

import (
	"container/list"
	"time"
)

// CacheI interface to cache
type CacheI interface {
	// Add inserts a new element v to cache
	Add(int64)

	// Update removes from cache expired data
	Update()

	// Size returns number of elements in cache
	Size() uint64
}

// Cache stores the number of requests in memory
type Cache struct {
	data         *list.List
	timeInterval time.Duration
}

// NewCache returns an initialized CounterCache
func NewCache(timeInterval time.Duration) *Cache {
	return &Cache{
		data:         list.New(),
		timeInterval: timeInterval,
	}
}

// Add inserts a new element v to cache
func (cache *Cache) Add(v int64) {
	cache.data.PushBack(v)
}

// Update removes from cache expired data
func (cache *Cache) Update() {
	curTime := time.Now().UnixNano()
	startFrom := curTime - cache.timeInterval.Nanoseconds()

	for cache.data.Len() > 0 {
		val, ok := cache.data.Front().Value.(int64)
		if ok && val >= startFrom {
			break
		}
		cache.data.Remove(cache.data.Front())
	}
}

// Size returns number of elements in cache
func (cache *Cache) Size() uint64 {
	return uint64(cache.data.Len())
}
