package counter

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"test-task/internal/pkg/counter"
	"test-task/internal/pkg/storage"
)

// StartApp starting counter-server
func StartApp() {
	flag.Parse()
	logger := log.New(os.Stdout, fmt.Sprintf("[%s] ", config.Name), log.LUTC|log.Ldate|log.Ltime)

	storage, err := storage.New(config.PathToDataFile)
	if err != nil {
		log.Fatalf("Can't create storage: %s\n", err)
	}
	srv := counter.NewServer(config.Port, config.ReadTimeout, config.WriteTimeout, logger, config.TimeInterval, storage)
	srv.InitCache()

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.Start(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Can't start server: %s\n", err)
		}
	}()
	// Graceful shutdown
	<-done
	log.Print("Stopping server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
	log.Print("Server stopped")
}
