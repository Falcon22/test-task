package storage

import (
	"encoding/binary"
	"io"
	"os"
	"strings"
)

// StorageI interface to local storage
type StorageI interface {
	// Insert inserts value into storage
	Insert(int64) error

	// GetSince returns all nodes since
	GetSince(int64) ([]int64, error)

	// Clear truncates file
	Clear() error
}

const int64Size = 8

// Storage stores the number of requests on disk
type Storage struct {
	file *os.File
}

// New returns an initialized Storage
func New(filePath string) (*Storage, error) {
	// Create dirs to file
	paths := strings.Split(filePath, "/")
	os.MkdirAll(strings.Join(paths[:len(paths)-1], ""), os.ModePerm)
	// Open or create file
	file, err := os.OpenFile(filePath, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0755)
	if err != nil {
		return nil, err
	}

	// Init storage
	return &Storage{
		file: file,
	}, nil
}

// Insert inserts value into storage
func (s *Storage) Insert(value int64) error {
	buf := make([]byte, int64Size)
	binary.LittleEndian.PutUint64(buf, uint64(value))
	_, err := s.file.Write(buf)
	if err != nil {
		return err
	}
	return nil
}

// GetSince returns all nodes since
func (s *Storage) GetSince(start int64) ([]int64, error) {
	buf := make([]byte, int64Size)
	res := make([]int64, 0)

	n, err := s.file.Read(buf)
	for err == nil && n == int64Size {
		value := int64(binary.LittleEndian.Uint64(buf))
		if value >= start {
			res = append(res, value)
		}
		n, err = s.file.Read(buf)
	}

	if err == io.EOF {
		return res, nil
	}
	return []int64{}, err
}

// Clear truncates file
func (s *Storage) Clear() error {
	if err := s.file.Truncate(0); err != nil {
		return err
	}
	if _, err := s.file.Seek(0, 0); err != nil {
		return err
	}
	return nil
}
