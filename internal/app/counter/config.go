package counter

import (
	"flag"
	"time"
)

type flags struct {
	Name         string
	Port         string
	WriteTimeout time.Duration
	ReadTimeout  time.Duration

	TimeInterval   time.Duration
	PathToDataFile string
}

var config flags

func init() {
	flag.StringVar(&config.Name, "project_name", "counter", "set name of project")
	flag.StringVar(&config.Port, "port", ":8080", "service port")
	flag.DurationVar(&config.WriteTimeout, "write_tmeout", 5*time.Second, "timeout for write")
	flag.DurationVar(&config.ReadTimeout, "read_timeout", 5*time.Second, "timeout for read")
	flag.DurationVar(&config.TimeInterval, "interval", time.Minute, "interval for window")
	flag.StringVar(&config.PathToDataFile, "data_file", "data/storage.dat", "path to file for counter storage")
}
